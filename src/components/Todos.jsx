import { useState } from 'react';
import data from '../data.json';
import "./Todos.scss";

const Todos = () => {

  const [todos, setTodos] = useState(data);

  const addTodo = (e) => {
    e.preventDefault();
    const newTodo = {
      id: new Date().getTime(),
      title: e.target.title.value,
      completed: false
    }
    setTodos([...todos, newTodo]);
  }

  const removeTodo = (todo) => {
    const currentTodo = todos.filter((t) => t.id !== todo.id);
    console.log(`Current todo: ${currentTodo}`);
  }

  const toggleComplete = () => { }

  return (
    <>
      <h1>Todo List</h1>
      <form onSubmit={addTodo}>
        <input type="text" name="title" />
        <button type="submit">Add Todo</button>
      </form>
      <div className='list-group'>
        { todos.map((todo) => (
            <div className='list-group-item' >
              <button onClick={() => toggleComplete(todo)}>{todo.complete ? 'Complete': 'Incomplete'}</button>
              <span>{todo.title}</span>
              <button onClick={() => removeTodo(todo)}>DELETE</button>
            </div>
        ))}
      </div>
    </>
  )
}

export default Todos;